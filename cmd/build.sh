#!/bin/bash

#This script is used to build the etherwake command itself.

#PATH to NDK folder:
#NDK=<enter path to ndk folder>
NDK=/home/the-scrabi/bin/Android/android-ndk-r10e

FILE=ether-wake.c
TARGET_DIR=../app/src/main/res/raw

FLAGS="-fPIC -pie"

$NDK/build/tools/make-standalone-toolchain.sh --platform=android-19 --install-dir=/tmp/ndk-arm --arch=arm
$NDK/build/tools/make-standalone-toolchain.sh --platform=android-19 --install-dir=/tmp/ndk-x86 --arch=x86
$NDK/build/tools/make-standalone-toolchain.sh --platform=android-19 --install-dir=/tmp/ndk-mips --arch=mips

CC_ARM=/tmp/ndk-arm/bin/arm-linux-androideabi-gcc
CC_X86=/tmp/ndk-x86/bin/i686-linux-android-gcc
CC_MIPS=/tmp/ndk-mips/bin/mipsel-linux-android-gcc

#mkdir $TARGET_DIR/arm
#mkdir $TARGET_DIR/x86
#mkdir $TARGET_DIR/mips

mkdir $TARGET_DIR

echo "$CC_ARM -o $TARGET_DIR/etherwake_arm $FILE $FLAGS"
$CC_ARM -o $TARGET_DIR/etherwake_arm $FILE $FLAGS
echo "$CC_X86 -o $TARGET_DIR/etherwake_i686 $FILE $FLAGS"
$CC_X86 -o $TARGET_DIR/etherwake_i686 $FILE $FLAGS
echo "$CC_MIPS -o $TARGET_DIR/etherwake_mips $FILE $FLAGS"
$CC_MIPS -o $TARGET_DIR/etherwake_mips $FILE $FLAGS
echo "DONE"
